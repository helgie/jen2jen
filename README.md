## jen2jen: a command line tool to backup and clone Jenkins setup

### Installation 

To install jen2jen from PyPi please run the following commands

`dude$ pip install jen2jen`

or if you want to build it from sources just clone the repo locally 

`dude$ git clone git@gitlab.com:helgie/jen2jen.git && cd jen2jen && virtualenv -p python3 venv && . venv/bin/actuvate && python setup.py install`

### Usage 

      jen2jen --version
      jen2jen submit [--host <jenkins url>] [--login <username>] [--password <password>] [--local <directory>]
      jen2jen fetch [--host <jenkins url>] [--login <username>] [--password <password>] [--local <directory>]
      jen2jen get-plugins [--host <jenkins url>] [--login <username>] [--password <password>] [--local <directory>]
      jen2jen post-plugins [--host <jenkins url>] [--login <username>] [--password <password>] [--local <directory>]

    Commands:
       fetch              Get the job structure from a remote Jenkins instance and store it locally.
       get-plugins        Get current set of plugins.
       post-plugins       Post current set of plugins.
       submit             Post the jobs from a local directory to some remote Jenkins instance.

    Options:
      -h --help                 Show this screen.
      --host=<hostname>         HTTP-address of a remote Jenkins instance. NB! :8080 may be needed.
      --login=<username>        A remote Jenkins administrator (or equal) account username.
      --password=<password>     A remote Jenkins administrator (or equal) account password.
      --local=<directory>     A remote Jenkins administrator (or equal) account password.


### Examples 

To fetch jobs setup from the Jenkins instance on the localhost to your home directory run the following command

`dude$ jen2jen fetch --login admin --password mypassword --host http://localhost:8080 --local ~/`

